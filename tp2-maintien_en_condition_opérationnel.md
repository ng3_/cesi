# Partie 3 : Maintien en condition opérationnelle

🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
[ngobert@proxy ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```

🌞 **Démarrez Netdata**

```bash
[ngobert@proxy ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[ngobert@proxy ~]$ sudo firewall-cmd --reload
success
[ngobert@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 4444/tcp 80/tcp 8888/tcp 443/tcp 19999/tcp
```

🌞 **Mettez en place un alerting Discord**

```bash
[root@proxy netdata]# ./edit-config health_alarm_notify.conf
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/918133613902565387/x22eY1D6UiikNUUAV9WMNGCHVsVMoHCwu4X1cki4lu5UC6GhEEcozk-6sXprITb5gXCm"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="netdata"
```

!netdate(https://gitlab.com/ng3_/cesi/-/blob/main/netdata.PNG)

🌞 **Ajustez la conf**

Ajout de la conf dans le fichier de configuraiton de nginx :

```bash
server {
        listen 19999;
        server_name web.tp2.cesi;

        #ssl_certificate "/etc/pki/tls/certs/web.tp2.cesi.crt";
        #ssl_certificate_key "/etc/pki/tls/private/web.tp2.cesi.key";
        location / {
            proxy_pass http://192.168.16.11:19999;
        }
     }
```


🌞 **Téléchargez et jouez avec Borg** sur la machine `web.tp2.cesi`

```bash
[ngobert@web ~]$ pip3 install borgbackup
[...]
[ngobert@web ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[ngobert@web ~]$ sudo chown root:root /usr/local/bin/borg
[ngobert@web ~]$ sudo chmod 755 /usr/local/bin/borg
```

🌞 **Ecrire un script**



🌞 **Créer un service**



🌞 **Créer un timer**




🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

