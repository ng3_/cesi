# TP3 : Docker


🌞 **Installez Docker en suivant [la doc officielle](https://docs.docker.com/)**

```bash
[ngobert@docker ~]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[ngobert@docker ~]$ sudo dnf update
[ngobert@docker ~]$ sudo dnf install -y docker-ce docker-ce-cli containerd.io
[ngobert@docker ~]$ docker --version
Docker version 20.10.11, build dea9396
```


🌞 **Setup Docker**

Ajout de l'user dans le groupe Docker :

```bash
[root@docker ~]# usermod -a -G docker,wheel ngobert
```

Après logout :

```bash
[ngobert@docker ~]$ groups
ngobert wheel docker
```

```bash
[ngobert@docker ~]$ sudo systemctl start docker.service
[sudo] password for ngobert:
[ngobert@docker ~]$ sudo systemctl enable docker.service
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
```

```bash
[ngobert@docker ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.11
```

🌞 **Lancez un conteneur NGINX**

Contneu du fichier du service :

```bash
[Unit]
Description=Docker Nginx

[Service]
ExecStart=docker run -d -p 8080:80 -v /home/ngobert/docker/index.html:/var/www/html/cesi/index.html -v /home/ngobert/docker/nginx.conf:/etc/nginx/nginx.conf --name nginx_redirect nginx

[Install]
WantedBy=multi-user.target
```

Reload systemctl et lancer le service :
```bash
[ngobert@docker docker]$ sudo systemctl daemon-reload
[ngobert@docker docker]$ sudo systemctl start docknginx.service
```

Conteneur lancé :

```bash
[ngobert@docker docker]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
885134dcf9d5   nginx     "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nginx_redirect
```
Curl sur le port 80 en localhost :
```bash
[ngobert@docker docker]$ curl http://127.0.0.1:8080/
Bonjour
```

🌞 **Vous devez accéder à votre page HTML, depuis votre navigateur en tapant `http://web.tp3.cesi`**

```bash
PS C:\Users\Nico> curl http://web.tp3.cesi:8080/


StatusCode        : 200
StatusDescription : OK
Content           : Bonjour

RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 8
                    Content-Type: text/html
                    Date: Thu, 09 Dec 2021 09:46:00 GMT
                    ETag: "61b1c1d7-8"
                    Last-Modified: Thu, 09 Dec 2021 08:4...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes],
                    [Content-Length, 8], [Content-Type, text/html]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 8
```

🌞 **Manipuler le conteneur qui tourne**

Récupérer un terminal dans le conteneur :

```bash
[ngobert@docker docker]$ docker exec -ti nginx_redirect sh
```

Récupérer l'adresse IP du conteneur :

```bash
[ngobert@docker docker]$ docker inspect -f '{{.NetworkSettings.IPAddress}}' nginx_redirect
172.17.0.2
```

Lancer un conteneur en limitant l'utilisation à 128M :

```bash
[ngobert@docker docker]$ docker run -tid --memory="128m" --name limited_memory alpine
```

Preuve : limité à 128MiB

```bash
[ngobert@docker docker]$ docker stats --no-stream | grep limited_memory
f5e7d67cc3a1   limited_memory   0.00%     380KiB / 128MiB       0.29%     866B / 0B         0B / 0B         1        0B / 0B         1
```


🌞 **Créer un réseau docker**

```bash
[ngobert@docker docker]$ docker network create wiki
705ba41f57d378e814277038d6fc98d9fc100ba2ad3119bfca9e982464e7d1c9
[ngobert@docker docker]$ docker network ls | grep wiki
705ba41f57d3   wiki      bridge    local

[ngobert@docker docker]$ docker inspect wiki
[
    {
        "Name": "wiki",
        "Id": "705ba41f57d378e814277038d6fc98d9fc100ba2ad3119bfca9e982464e7d1c9",
        "Created": "2021-12-09T11:04:45.838775562+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
```


🌞 **Lancez un conteneur MySQL**

Contenu du fichier dockmysql.service :
```bash
[Unit]
Description=Docker mysql

[Service]
ExecStart=docker run -tid --network wiki --env-file /home/ngobert/docker/mysql-variable --name mysql-server mysql

[Install]
WantedBy=multi-user.target
```

Contenu du fichier de variables :

```bash
MYSQL_ROOT_PASSWORD=password
MYSQL_ALLOW_EMPTY_PASSWORD=false
MYSQL_RANDOM_ROOT_PASSWORD=false
MYSQL_USER=ngobert
MYSQL_PASSWORD=password
MYSQL_DATABASE=wiki
```

Preuves de fonctionnement, shell dans le routeur :

```bash
[ngobert@docker system]$ docker exec -ti mysql-server bash
root@e9699ac25fed:/# mysql -u ngobert -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| wiki               |
+--------------------+
2 rows in set (0.00 sec)
```


🌞 **Lancez un conteneur [WikiJS](https://js.wiki/)**

Fichier des variables :

```bash
DB_TYPE=mysql
DB_HOST=172.18.0.2
DB_PORT=3306
DB_USER=ngobert
DB_PASS=password
DB_NAME=wiki
```

```bash
[ngobert@docker docker]$ docker run -tid -p 8080:3000 --network wiki --env-file /home/ngobert/docker/wikijs-variable --name wikijs requarks/wiki:2
c353448d2ab70212a09550bd16d9ea679271933138bd96059be2e7d4182494ed
[ngobert@docker docker]$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS         PORTS
NAMES
c353448d2ab7   requarks/wiki:2   "docker-entrypoint.s…"   3 seconds ago    Up 1 second    3443/tcp, 0.0.0.0:8080->3000/tcp, :::8080->3000/tcp   wikijs
6c67ebebe533   mysql             "docker-entrypoint.s…"   14 minutes ago   Up 8 minutes   3306/tcp, 33060/tcp
mysql-server
```

Test :

```bash
PS C:\Users\Nico> curl http://web.tp3.cesi:8080/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html><html><head><meta
                    http-equiv="X-UA-Compatible"
                    content="IE=edge"><meta charset="UTF-8"><meta
                    name="viewport" content="user-scalable=yes,
                    width=device-width, initial-scale=1, maximum-sca...
RawContent        : HTTP/1.1 200 OK
                    Vary: Accept-Encoding
                    Connection: keep-alive
                    Keep-Alive: timeout=5
                    Content-Length: 1315
                    Content-Type: text/html; charset=utf-8
                    Date: Thu, 09 Dec 2021 11:48:51 GMT
                    ETag: W/"523-y...
Forms             : {}
Headers           : {[Vary, Accept-Encoding], [Connection,
                    keep-alive], [Keep-Alive, timeout=5],
                    [Content-Length, 1315]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 1315
```

🌞 **Créez une image**

On récupère l'image officielle de Debian :

```bash
[ngobert@docker docker]$ docker pull debian
[ngobert@docker docker]$ docker images |grep debian
debian          latest    05d2318291e3   7 days ago    124MB
[ngobert@docker docker]$ docker run -tid -p 8090:80 --name my_debian debian
5421d3be95aade741f55a2f341bb5c6a49c4b848b377444a8305681d946e3c9d
[ngobert@docker docker]$ docker exec my_debian bash -c "apt update -y && apt install -y python3 && python3 -m http.server 8888"

```

🌞 **Test**



🌞 **Installez `docker-compose`** en suivant la doc officielle

```bash
[ngobert@docker docker]$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
[ngobert@docker docker]$ sudo chmod +x /usr/local/bin/docker-compose
[ngobert@docker docker]$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
[ngobert@docker docker]$ docker-compose --version
docker-compose version 1.29.2, build 5becea4c
```


🌞 **Lancez un `docker-compose.yml` de test**

Contenu du docker-compose.yml :

```bash
version: "3.5"

services:
  web:
    nginx:
      image: nginx
      ports: 8080:80
      networks:
        test-net:

networks:
  test-net:
```

🌞 **Vérifier**

 ```bash
[ngobert@docker docker-compose]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
e04d217803d8   nginx     "/docker-entrypoint.…"   2 minutes ago    Up 2 minutes    0.0.0.0:8080->80/tcp, :::8080->80/tcp   docker-compose_web_1
```

Avec docker-compose ps :

```bash
[ngobert@docker docker-compose]$ docker-compose ps
        Name                 Command         State          Ports
------------------------------------------------------------------------docker-compose_web_1   /docker-              Up      0.0.0.0:8080->80/tc                       entrypoint.sh ngin            p,:::8080->80/tcp
```

Test :

```bash
PS C:\Users\Nico> curl http://web.tp3.cesi:8080/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                    <title>Welcome to nginx!</title>
                    <style>
                    html { color-scheme: light dark; }
                    body { width: 35em; margin: 0 auto;
                    font-family: Tahoma, Verdana, Arial, sans-serif; }
                    </style...
RawContent        : HTTP/1.1 200 OK
```

🌞 **Dans le même fichier**

```bash
version: "3.5"

services:
  web:
    image: nginx
    ports:
      - '8080:80'
    networks:
      test-net:
  sleep:
    image: debian
    netorks:
      test-net:
    command: sleep 99999

networks:
  test-net:
```

Résultat docker-compose ps :

```bash
[ngobert@docker docker-compose]$ docker-compose ps
        Name                 Command         State          Ports
------------------------------------------------------------------------docker-                sleep 99999           Up
compose_sleep_1
docker-compose_web_1   /docker-              Up      0.0.0.0:8080->80/tc                       entrypoint.sh ngin            p,:::8080->80/tcp
```

```bash
[ngobert@docker docker-compose]$ docker exec -ti docker-compose_sleep_1 bash
root@7a251a29d773:/#
root@7a251a29d773:/# ping docker-compose_web_1
PING docker-compose_web_1 (172.20.0.2) 56(84) bytes of data.
64 bytes from docker-compose_web_1.docker-compose_test-net (172.20.0.2): icmp_seq=1 ttl=64 time=0.112 ms
64 bytes from docker-compose_web_1.docker-compose_test-net (172.20.0.2): icmp_seq=2 ttl=64 time=0.064 ms
^C64 bytes from 172.20.0.2: icmp_seq=3 ttl=64 time=0.105 ms
```

🌞 **Variables**

```bash
[ngobert@docker docker-compose]$ sudo vi .env
NGINX_PORT=8080
```

Modification du docker-compose.yml :

```bash
version: "3.5"

services:
  web:
    image: nginx
    ports:
      - "${NGINX_PORT}:80"
    networks:
      test-net:
  sleep:
    image: debian
    networks:
      test-net:
    command: sleep 99999

networks:
  test-net:
```

```bash
[ngobert@docker docker-compose]$ docker-compose ps
        Name                 Command         State          Ports
------------------------------------------------------------------------docker-                sleep 99999           Up
compose_sleep_1
docker-compose_web_1   /docker-              Up      0.0.0.0:8080->80/tc                       entrypoint.sh ngin            p,:::8080->80/tcp
```

**Nextcloud**

🌞 **Créez un docker-compose.yml**

Contneu du répertoire "nextcloud" :

```bash
[ngobert@docker nextcloud]$ tree
.
├── data
├── db
├── docker-compose.yml
└── nginx
    ├── nginx.conf
    ├── web.tp3.cesi.crt
    └── web.tp3.cesi.key
```



🌞 **Lancez le `docker-compose.yml` modifié**



🌞 **Variables**



🌞 **Créez un `docker-compose.yml`**


