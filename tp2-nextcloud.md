# NEXTCLOUD

## Configuration des machines
web.tp2.cesi - 192.168.16.11/24

db.tp2.cesi  - 192.168.16.12/24


🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**
```bash
[ngobert@db ~]$ sudo dnf install mariadb-server
...
[ngobert@db ~]$ mysql --version
mysql  Ver 15.1 Distrib 10.3.28-MariaDB, for Linux (x86_64) using readline 5.1
```

🌞 **Le service MariaDB**
```bash
[ngobert@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[ngobert@db ~]$ sudo systemctl status mariadb.service
mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 12:37:17 CET; 4s ago

[ngobert@db ~]$ ss -luntp
Netid State  Recv-Q Send-Q Local Address:Port  Peer Address:Port Process
tcp   LISTEN 0      128          0.0.0.0:4444       0.0.0.0:*
tcp   LISTEN 0      80                 *:3306             *:*
tcp   LISTEN 0      128             [::]:4444          [::]:*

[ngobert@db ~]$ ps -ef | grep mariadb
ngobert     3751    1643  0 12:40 pts/1    00:00:00 grep --color=auto mariadb
```


🌞 **Firewall**
```bash
[ngobert@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
[sudo] password for ngobert:
success
[ngobert@db ~]$ sudo firewall-cmd --reload
success
[ngobert@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 4444/tcp 80/tcp 8888/tcp 443/tcp 3306/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **Configuration élémentaire de la base**
```bash
[ngobert@db ~]$ sudo mysql_secure_installation

[...]

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyoneto log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**
```bash
[ngobert@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> CREATE USER 'nextcloud'@'192.168.16.11' IDENTIFIED BY 'ncpassword';

MariaDB [(none)]>CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

MariaDB [(none)]>GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'192.168.16.11';

MariaDB [(none)]>FLUSH PRIVILEGES;
```

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**
```bash
[ngobert@web ~]$ sudo dnf provides mysql
[sudo] password for ngobert:
Last metadata expiration check: 1:28:58 ago on Tue 07 Dec 2021 11:27:21 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs
                                                  : and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```

🌞 **Tester la connexion**
```bash
[ngobert@web ~]$ sudo dnf provides mysql
[sudo] password for ngobert:
Last metadata expiration check: 1:28:58 ago on Tue 07 Dec 2021 11:27:21 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs
                                                  : and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[ngobert@web ~]$ sudo dnf install mysql
Last metadata expiration check: 1:30:01 ago on Tue 07 Dec 2021 11:27:21 AM CET.
Dependencies resolved.
=========================================================================
 Package     Arch   Version                              Repo       Size
=========================================================================
Installing:
 mysql       x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream  12 M
Installing dependencies:
 mariadb-connector-c-config
             noarch 3.1.11-2.el8_3                       appstream  14 k
 mysql-common
             x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream 133 k
Enabling module streams:
 mysql              8.0

Transaction Summary
=========================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M

[ngobert@web ~]$ mysql -u nextcloud -h 192.168.16.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 18
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> use nextcloud;
Database changed
mysql> show tables;
Empty set (0.00 sec)

mysql>
```

🌞 **Installer Apache sur la machine `web.tp2.cesi`**
```bash
[ngobert@web ~]$ sudo dnf install httpd
[ngobert@web ~]$ httpd -v
Server version: Apache/2.4.37 (rocky)
Server built:   Nov 15 2021 03:12:26
```

🌞 **Analyse du service Apache**

```bash
[ngobert@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[ngobert@web ~]$ sudo systemctl start httpd
[ngobert@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendo>   Active: active (running) since Tue 2021-12-07 13:04:07 CET; 4s ago
     Docs: man:httpd.service(8)
```

Le port d'écoute Apache par défaut est le 80

```bash
[ngobert@web ~]$ ss -lutpn
Netid State  Recv-Q Send-Q Local Address:Port  Peer Address:Port Process
tcp   LISTEN 0      128          0.0.0.0:4444       0.0.0.0:*
tcp   LISTEN 0      128                *:80               *:*
tcp   LISTEN 0      128             [::]:4444          [::]:*
```

Les processus Apache sont lancés par le user "apache"

```bash
[ngobert@web ~]$ ps -ef | grep apache
apache      3182    3181  0 13:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3183    3181  0 13:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3184    3181  0 13:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3185    3181  0 13:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

🌞 **Un premier test**

```bash
[ngobert@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
Warning: ALREADY_ENABLED: 80:tcp
success
```

Curl :

```bash
PS C:\Users\Nico> curl 192.168.16.11
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after
it has been installed on a Rocky Linux system. [...]
```

Navigateur :
!apache_gui(https://gitlab.com/ng3_/cesi/-/blob/main/apache_gui.PNG)


🌞 **Installer PHP**
```bash
[ngobert@web ~]$ sudo dnf install epel-release

[ngobert@web ~]$ sudo dnf update

[ngobert@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

[ngobert@web ~]$ sudo dnf module enable php:remi-7.4

[ngobert@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

🌞 **Analyser la conf Apache**
```bash
[ngobert@web conf]$ sudo vi httpd.conf

[ngobert@web httpd]$ cd conf.d/
[ngobert@web conf.d]$ ll
total 20
-rw-r--r--. 1 root root 2926 Nov 15 04:13 autoindex.conf
-rw-r--r--. 1 root root 1681 Nov 16 17:40 php74-php.conf
-rw-r--r--. 1 root root  400 Nov 15 04:13 README
-rw-r--r--. 1 root root 1252 Nov 15 04:10 userdir.conf
-rw-r--r--. 1 root root  574 Nov 15 04:10 welcome.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**
```bash
[ngobert@web conf.d]$ sudo touch nextcloud.conf
[ngobert@web conf.d]$ sudo vi nextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[ngobert@web conf.d]$ [ngobert@web conf.d]$ sudo systemctl reload httpd
```

🌞 **Configurer la racine web**
```bash
[ngobert@web conf.d]$ sudo mkdir /var/www/nextcloud && sudo mkdir /var/ww
w/nextcloud/html

[ngobert@web conf.d]$ sudo chown -R apache:apache /var/www/nextcloud/
[ngobert@web conf.d]$ ll /var/www/nextcloud/
total 0
drwxr-xr-x. 2 apache apache 6 Dec  7 13:52 html

[ngobert@web conf.d]$ sudo timedatectl
               Local time: Tue 2021-12-07 13:54:31 CET
           Universal time: Tue 2021-12-07 12:54:31 UTC
                 RTC time: Tue 2021-12-07 12:54:31
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no


🌞 **Configurer PHP**

```bash
[ngobert@web php74]$ sudo vi php.ini
[...]
date.timezone = "Europe/Paris"
```

🌞 **Récupérer Nextcloud**
```bash
[ngobert@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[...]
[ngobert@web ~]$ ll
total 151604
-rw-rw-r--. 1 ngobert ngobert 155240687 Dec  7 14:04 nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**
```bash
[ngobert@web ~]$ sudo unzip nextcloud-21.0.1.zip
[...]
[ngobert@web ~]$ ll
total 151608
drwxr-xr-x. 13 root    root         4096 Apr  8  2021 nextcloud
-rw-rw-r--.  1 ngobert ngobert 155240687 Dec  7 14:04 nextcloud-21.0.1.zip

[ngobert@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/
[ngobert@web ~]$ ll /var/www/nextcloud/html/
total 4
drwxr-xr-x. 13 root root 4096 Apr  8  2021 nextcloud

[ngobert@web ~]$ sudo chown -R apache:apache /var/www/nextcloud/html/
[ngobert@web ~]$ ll /var/www/nextcloud/html/
total 4
drwxr-xr-x. 13 apache apache 4096 Apr  8  2021 nextcloud

[ngobert@web ~]$ sudo rm -f nextcloud-21.0.1.zip
```

🌞 **Modifiez le fichier `hosts` de votre PC**

Fichier host de ma machine hôte :
```bash
#
127.0.0.1 localhost
::1 localhost

192.168.16.11 web.tp2.cesi
```

Test de la résolution DNS :
```bash 
PS C:\Users\Nico> ping web.tp2.cesi

Envoi d’une requête 'ping' sur web.tp2.cesi [192.168.16.11] avec 32 octets de données :
Réponse de 192.168.16.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.16.11:
    Paquets : envoyés = 1, reçus = 1, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```


🌞 **Tester l'accès à NextCloud et finaliser son install'**
!config_nextcloud(https://gitlab.com/ng3_/cesi/-/blob/main/conf_nextcloud.PNG)
