## NEXTCLOUD

# Partie 2 : Sécurisation

🌞 **Modifier la conf du serveur SSH**

Voir le fichier de /etc/ssh/sshd_conf

```bash
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
[...]
PermitRootLogin no
[...]
PasswordAuthentication no
```

🌞 **Installez et configurez fail2ban**

```bash
[root@web ~]# sudo dnf install fail2ban
[root@web ~]# systemctl enable fail2ban
[root@web ~]# systemctl start fail2ban
[root@web ~]# systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; 
   Active: active (running) since Tue 2021-12-07 14:57:36 CET; 3s
```

Configuration de fail2ban :

```bash
[ngobert@web ~]$ fail2ban-server --version
Fail2Ban v0.11.2
[ngobert@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

Contenu du fichier de conf pour ssh :

```bash
[ngobert@web ~]$ sudo vi /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
port = ssh
action = iptables-multiport
logpath = /var/log/secure
maxretry = 5
bantime = 600
```
Restart des services :

```bash
[ngobert@web fail2ban]$ sudo systemctl restart fail2ban
```

Test du service :

```bash
[ngobert@web fail2ban]$ sudo iptables -L -n
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
f2b-sshd   tcp  --  0.0.0.0/0            0.0.0.0/0            multiport dports 22

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination

Chain f2b-sshd (1 references)
target     prot opt source               destination
REJECT     all  --  192.168.16.12        0.0.0.0/0            reject-with icmp-port-unreachable
RETURN     all  --  0.0.0.0/0            0.0.0.0/0
```

Ban une IP à la main 

```bash
[ngobert@web fail2ban]$ sudo fail2ban-client set sshd banip 10.10.10.10
1
[ngobert@web fail2ban]$ sudo iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
f2b-sshd   tcp  --  anywhere             anywhere             multiport dports ssh

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination

Chain f2b-sshd (1 references)
target     prot opt source               destination
REJECT     all  --  10.10.10.10          anywhere             reject-with icmp-port-unreachable

REJECT     all  --  192.168.16.12        anywhere             reject-with icmp-port-unreachable
```

Pour unban une IP :

```bash
[ngobert@web fail2ban]$ sudo fail2ban-client set sshd unbanip 10.10.10.10
```


🌞 **Installer NGINX**

Adresse IP de proxy.tp2.cesi -> 192.168.16.10/24

```bash
[ngobert@proxy ~]$ sudo dnf install nginx
```

🌞 **Configurer NGINX comme reverse proxy**

Fichier de configuration nginx sur le proxy :

```bash
    server {
        server_name nextcloud.tp2.cesi;

        location / {
            proxy_pass http://192.168.16.11;
        }
```

Test :

```bash
PS C:\Users\Nico> curl http://192.168.16.10/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php";
                    </script>
                        <meta http-equiv="refresh" content="0;
                    URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
```

🌞 **Une fois en place, text !**

Contenu du fichier hosts de ma machine hôte : 

```bash
#
127.0.0.1 localhost
::1 localhost

192.168.16.10 web.tp2.cesi
```

Test curl : 

```bash
PS C:\Users\Nico> curl http://web.tp2.cesi/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php";
                    </script>
                        <meta http-equiv="refresh" content="0;
                    URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
```

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
[ngobert@proxy ~]$ sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
[sudo] password for ngobert:
Generating a RSA private key
...................................................................................................................................................................................................................................................................................................................................++++
..++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:France
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:
[ngobert@proxy ~]$ ls
server.crt  server.key
```

🌞 **Allez, faut ranger la chambre**

Placer le certificat et la clé dans les bons répertoires :

```bash
[ngobert@proxy ~]$ sudo mv server.key /etc/pki/tls/private/ && sudo mv server.crt /etc/pki/tls/certs/
```

Renommer le certificat et la clé :

```bash
[ngobert@proxy ~]$ sudo mv /etc/pki/tls/private/server.key /etc/pki/tls/private/web.tp2.cesi.key && sudo mv /etc/pki/tls/certs/server.crt /etc/pki/tls/certs/web.tp2.cesi.crt
```

🌞 **Affiner la conf de NGINX**

```bash
server {
        listen 443 ssl http2;
        server_name nextcloud.tp2.cesi;

        ssl_certificate "/etc/pki/tls/certs/web.tp2.cesi.crt";
        ssl_certificate_key "/etc/pki/tls/private/web.tp2.cesi.key";
        location / {
            proxy_pass http://192.168.16.11;
        }

        location index.html {
            deny all;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```

🌞 **Test !**

```bash
C:\Users\Nico>curl --insecure https://web.tp2.cesi/index.html
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>

C:\Users\Nico>curl --insecure https://web.tp2.cesi/index.php

C:\Users\Nico>
C:\Users\Nico>curl --insecure https://web.tp2.cesi/index.php/login
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="KCJ0CcetiVhIS4nJExDe72fzNDiMnZPp+/VXYLF3Sts=:cmkOQ+zYxDN5eOO+InawrCi/AwD/3KGfqa8OVNo0JqI=">
```


🌞 **Bonus**

