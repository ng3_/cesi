🌞 Go `vagrant up` !

```powershell
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant status
Current machine states:

default                   running (virtualbox)
[...]
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant ssh
[vagrant@localhost ~]
[vagrant@localhost ~]$ vim --version
VIM - Vi IMproved 7.4 (2013 Aug 10, compiled Dec 15 2020 16:44:08)
```


🌞 Modifier le Vagrantfile (et/ou le `setup.sh`) 

```sh
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# Installation d'un paquet
yum install -y vim
yum install -y ansible

# On active la connexion au serveur SSH avec un mot de passe
# Ui c'est nul, mais on en a besoin ensuite
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

```powershell
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant package --output centos-ansible.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/Nico/Documents/Labo/Fichiers Vagrant/CentOSCesi/centos-ansible.box

PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant box add centos-ansible centos-ansible.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos-ansible' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/Nico/Documents/Labo/Fichiers%20Vagrant/CentOSCesi/centos-ansible.box
    box:
==> box: Successfully added box 'centos-ansible' (v0) for 'virtualbox'!
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSCesi> vagrant box list
centos-ansible  (virtualbox, 0)
centos-cesi     (virtualbox, 0)
centos/7        (virtualbox, 2004.01)
ubuntu/focal64  (virtualbox, 20210709.0.0)
ubuntu/focal64  (virtualbox, 20210803.0.0)
ubuntu/xenial64 (virtualbox, 20210721.0.0)
```

```powershell
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSAnsible> vagrant init
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSAnsible> notepad .\Vagrantfile
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\CentOSAnsible> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos-ansible'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: CentOSAnsible_default_1639130086733_32762
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
[default] GuestAdditions 6.1.28 running --- OK.
==> default: Checking for guest additions in VM...
==> default: Mounting shared folders...
    default: /vagrant => C:/Users/Nico/Documents/Labo/Fichiers Vagrant/CentOSAnsible
```


🌞 **Test !**

```powershell
[vagrant@localhost ~]$ vim --version
VIM - Vi IMproved 7.4 (2013 Aug 10, compiled Dec 15 2020 16:44:08)
[...]
[vagrant@localhost ~]$ ansible --version
ansible 2.9.25
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/vagrant/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.5 (default, Nov 16 2020, 22:23:17) [GCC 4.8.5 20150623 (Red Hat 4.8.5-44)]
```


🌞 Ecrivez un `Vagrantfile` qui déploie 3 machines dans le même réseau

```powershell
Vagrant.configure("2") do |config|
  
  # Configuration commune à toutes les machines
  config.vm.box = "centos-ansible"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 
  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
  end

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "10.10.10.1"
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "10.10.10.2"
  end

  # Config une première VM "node3"
  config.vm.define "node3" do |node3|
    node3.vm.network "private_network", ip: "10.10.10.3"
  end
end
```

```powershell
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\3hotes> vagrant up
Bringing machine 'node1' up with 'virtualbox' provider...
Bringing machine 'node2' up with 'virtualbox' provider...
Bringing machine 'node3' up with 'virtualbox' provider...
==> node1: You assigned a static IP ending in ".1" to this machine.
==> node1: This is very often used by the router and can cause the
==> node1: network to not work properly. If the network doesn't work
==> node1: properly, try changing this IP.
==> node1: Importing base box 'centos-ansible'...
==> node1: Matching MAC address for NAT networking...
==> node1: You assigned a static IP ending in ".1" to this machine.
==> node1: This is very often used by the router and can cause the
==> node1: network to not work properly. If the network doesn't work
==> node1: properly, try changing this IP.
==> node1: Setting the name of the VM: 3hotes_node1_1639130411731_67119
==> node1: Fixed port collision for 22 => 2222. Now on port 2201.
==> node1: Clearing any previously set network interfaces...
==> node1: Preparing network interfaces based on configuration...
    node1: Adapter 1: nat
    node1: Adapter 2: hostonly
==> node1: Forwarding ports...
    node1: 22 (guest) => 2201 (host) (adapter 1)
    node1: SSH address: 127.0.0.1:2201
    node1: SSH username: vagrant
    node1: SSH auth method: private key
==> node1: Machine booted and ready!
==> node1: Checking for guest additions in VM...
==> node1: Configuring and enabling network interfaces...
==> node2: Importing base box 'centos-ansible'...
==> node2: Matching MAC address for NAT networking...
==> node2: Setting the name of the VM: 3hotes_node2_1639130452364_33330
==> node2: Fixed port collision for 22 => 2222. Now on port 2202.
==> node2: Clearing any previously set network interfaces...
==> node2: Preparing network interfaces based on configuration...
    node2: Adapter 1: nat
    node2: Adapter 2: hostonly
==> node2: Forwarding ports...
    node2: 22 (guest) => 2202 (host) (adapter 1)
==> node2: Booting VM...
==> node2: Waiting for machine to boot. This may take a few minutes...
    node2: SSH address: 127.0.0.1:2202
    node2: SSH username: vagrant
    node2: SSH auth method: private key
==> node2: Machine booted and ready!
==> node2: Checking for guest additions in VM...
==> node2: Configuring and enabling network interfaces...
==> node3: Importing base box 'centos-ansible'...
==> node3: Matching MAC address for NAT networking...
==> node3: Setting the name of the VM: 3hotes_node3_1639130484545_68200
==> node3: Fixed port collision for 22 => 2222. Now on port 2203.
==> node3: Clearing any previously set network interfaces...
==> node3: Preparing network interfaces based on configuration...
    node3: Adapter 1: nat
    node3: Adapter 2: hostonly
==> node3: Forwarding ports...
    node3: 22 (guest) => 2203 (host) (adapter 1)
==> node3: Booting VM...
==> node3: Waiting for machine to boot. This may take a few minutes...
    node3: SSH address: 127.0.0.1:2203
    node3: SSH username: vagrant
    node3: SSH auth method: private key
==> node3: Machine booted and ready!
==> node3: Checking for guest additions in VM...
==> node3: Configuring and enabling network interfaces...
```

Connexion à chaque noeuds :
```powershell
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\3hotes> vagrant ssh node1
Last login: Fri Dec 10 10:05:02 2021 from 10.0.2.2
[vagrant@localhost ~]$ logout
Connection to 127.0.0.1 closed.
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\3hotes> vagrant ssh node2
Last login: Fri Dec 10 10:05:25 2021 from 10.0.2.2
[vagrant@localhost ~]$ logout
Connection to 127.0.0.1 closed.
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\3hotes> vagrant ssh node3
Last login: Fri Dec 10 10:05:37 2021 from 10.0.2.2
[vagrant@localhost ~]$
```

Test des ping :

```bash
[vagrant@localhost ~]$ ping 10.10.10.2
PING 10.10.10.2 (10.10.10.2) 56(84) bytes of data.
64 bytes from 10.10.10.2: icmp_seq=1 ttl=64 time=0.356 ms
64 bytes from 10.10.10.2: icmp_seq=2 ttl=64 time=0.563 ms
^C
--- 10.10.10.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.356/0.459/0.563/0.105 ms
[vagrant@localhost ~]$ ping 10.10.10.3
PING 10.10.10.3 (10.10.10.3) 56(84) bytes of data.
64 bytes from 10.10.10.3: icmp_seq=1 ttl=64 time=0.486 ms
64 bytes from 10.10.10.3: icmp_seq=2 ttl=64 time=0.467 ms
^C
--- 10.10.10.3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.467/0.476/0.486/0.023 ms
[vagrant@localhost ~]$ logout
Connection to 127.0.0.1 closed.
PS C:\Users\Nico\Documents\Labo\Fichiers Vagrant\3hotes> vagrant ssh node2
Last login: Fri Dec 10 10:06:02 2021 from 10.0.2.2
[vagrant@localhost ~]$ ping 10.10.10.3
PING 10.10.10.3 (10.10.10.3) 56(84) bytes of data.
64 bytes from 10.10.10.3: icmp_seq=1 ttl=64 time=0.408 ms
^C
--- 10.10.10.3 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.408/0.408/0.408/0.000 ms
```

Connexion SSH noeud à noeud :

```bash
[vagrant@localhost ~]$ ssh vagrant@10.10.10.1
The authenticity of host '10.10.10.1 (10.10.10.1)' can't be established.
ECDSA key fingerprint is SHA256:sSjOBP5AEJOB5q1gQeRtMSaa7PLpsQ/FN5yZshAvyPU.
ECDSA key fingerprint is MD5:4d:9e:52:24:1b:93:dc:80:46:94:6e:e9:46:b2:32:89.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.10.10.1' (ECDSA) to the list of known hosts.
vagrant@10.10.10.1's password:
Last login: Fri Dec 10 10:07:08 2021 from 10.0.2.2
[vagrant@localhost ~]$ ssh vagrant@10.10.10.2
The authenticity of host '10.10.10.2 (10.10.10.2)' can't be established.
ECDSA key fingerprint is SHA256:sSjOBP5AEJOB5q1gQeRtMSaa7PLpsQ/FN5yZshAvyPU.
ECDSA key fingerprint is MD5:4d:9e:52:24:1b:93:dc:80:46:94:6e:e9:46:b2:32:89.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.10.10.2' (ECDSA) to the list of known hosts.
vagrant@10.10.10.2's password:
Last login: Fri Dec 10 10:09:23 2021 from 10.0.2.2
[vagrant@localhost ~]$
```

🌞 **Mettez en place une connexion SSH sans mot de passe**


Génération de la paire de clé sur le master ansible :

```
[vagrant@admin ansible]$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/vagrant/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/vagrant/.ssh/id_rsa.
Your public key has been saved in /home/vagrant/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:VtHh/x0vXlLhEqHdUGR2gRNZd4rUk0CBcy4KQEpKjUw vagrant@admin.tp4.cesi
The key's randomart image is:
+---[RSA 2048]----+
|oE+..     .+*OBO=|
|.= o.     o+==X.+|
|. .  .    .=ooo+ |
|      .  .. ..o .|
|       .S. . ..o.|
|       ..     .o+|
|              o =|
|             . + |
|              .  |
+----[SHA256]-----+
```

Déploiement des clés sur les noeuds :

```bash
[vagrant@admin ansible]$ ssh-copy-id vagrant@10.10.10.3
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/vagrant/.ssh/id_rsa.pub"
The authenticity of host '10.10.10.3 (10.10.10.3)' can't be established.
ECDSA key fingerprint is SHA256:sSjOBP5AEJOB5q1gQeRtMSaa7PLpsQ/FN5yZshAvyPU.
ECDSA key fingerprint is MD5:4d:9e:52:24:1b:93:dc:80:46:94:6e:e9:46:b2:32:89.
Are you sure you want to continue connecting (yes/no)? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
vagrant@10.10.10.3's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'vagrant@10.10.10.3'"
and check to make sure that only the key(s) you wanted were added.
```

Même opération pour le noeud1.

Test sur node1:
```bash
[vagrant@admin ansible]$ ssh vagrant@10.10.10.2
Last failed login: Fri Dec 10 10:20:37 UTC 2021 from 10.10.10.1 on ssh:notty
There was 1 failed login attempt since the last successful login.
Last login: Fri Dec 10 10:13:53 2021 from 10.0.2.2
[vagrant@node1 ~]$
```

🌞 **Créez vous un répertoire de travail**

```bash
[vagrant@admin ansible]$ pwd
/home/vagrant/ansible
```

🌞 **Créez un playbook minimaliste `nginx.yml` :**

```bash
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

```bash
[vagrant@admin ansible]$ ll
total 4
-rw-rw-r--. 1 vagrant vagrant 414 Dec 10 10:23 nginx.yml
```


🌞 **Et créez un inventaire `hosts.ini` :**

Contenu du hosts.ini

```bash
[cesi]
10.10.10.2
10.10.10.3
```


🌞 **Enfin, créez un fichier `index.html.j2` :**

Contenu du index.html.j2

```bash
Hello from {{ ansible_default_ipv4.address }}
```


🌞 **Lancez le playbook !**

```bash
[vagrant@admin ansible]$ ansible-playbook -i hosts.ini nginx.yml

PLAY [Install nginx] ***********************************************************

TASK [Gathering Facts] *********************************************************
ok: [10.10.10.2]
ok: [10.10.10.3]

TASK [Add epel-release repo] ***************************************************
ok: [10.10.10.3]
ok: [10.10.10.2]

TASK [Install nginx] ***********************************************************


changed: [10.10.10.3]
changed: [10.10.10.2]

TASK [Insert Index Page] *******************************************************
changed: [10.10.10.2]
changed: [10.10.10.3]

TASK [Start NGiNX] *************************************************************
changed: [10.10.10.3]
changed: [10.10.10.2]

PLAY RECAP *********************************************************************
10.10.10.2                 : ok=5    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.10.10.3                 : ok=5    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

🌞 **Créez un playbook `apache.yml` qui :**

```yml
---
- name: Install MariaDB
  hosts: cesi
  become: true

  vars:
    mysql_root_password: "password"

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install MariaDB
    yum:
      name: mariadb-server
      state: present

  - name: Start MartiaDB
    service:
      name: mariadb
      enabled: true
      state: started

  - name: mysql_root_password
    mysql_user:
      login_user: root
      login_password: "{{ mysql_root_password }}"
      user: root
      check_implicit_admin: true
      password: "{{ mysql_root_password }}"
      host: localhost
```

🌞 **Puis un playbook `users.yml` qui :**

```yml
---
- name: Add admin
  hosts: cesi
  become: true

tasks:
  - name: "Create user accounts and add users to groups"
    user:
      name: "admin"
      groups: "admin, sudoers"
  - name: "Add authorized keys"
    authorized_key:
      user: "admin"
      key: "{{ lookup('file', '/home/admin/.ssh/id_rsa.pub) }}"
  - name: "Allow admin users to sudo without a password"
    lineinfile:
      dest: "/etc/sudoers"
      state: "present"
      regexp: "^%admin"
      line: "%admin ALL=(ALL) NOPASSWD: ALL"
```

🌞 **Enfin un playbook `firewall.yml` qui :**

```bash
[vagrant@admin ansible]$ cat firewall.yml
---
- name: Conf FW
  hosts: cesi
  become: true

tasks:
  - name: enable 80
    firewalld:
      zone: public
      port: 80/tcp
      permanent: true
      state: enabled
    become: yes
  - name: enable 22
    firewalld:
      zone: public
      port: 22/tcp
      permanent: true
      state: enabled
    become: yes
```

```yml
---
- name: Install MariaDB
  hosts: cesi
  become: true

  vars:
    mysql_root_password: "password"

  tasks:
    - name: Add epel-release repo
      yum:
        name: epel-release
        state: present

    - name: Install MariaDB
      yum:
        name:
          - mariadb-server
          - python3
          - python2-PyMySQL
        state: present

    - name: Start MartiaDB
      service:
        name: mariadb
        enabled: true
        state: started

    - name: mysql_root_password
      mysql_user:
        login_user: root
        login_password: "{{ mysql_root_password }}"
        user: root
        check_implicit_admin: true
        password: "{{ mysql_root_password }}"
        host: localhost

    - name: Create user accounts and add users to groups
      ansible.builtin.user:
        name: admin

    - name: Add authorized keys
      authorized_key:
        user: "admin"
```

🌞 **Ré-exécutez vos playbooks et assurez qu'il n'y a que des `ok:`**




🌞 Trouver une commande qui permet de tester la bonne syntaxe d'un 



🌞 Trouver une commande qui permet de tester la bonne syntaxe d'un fichier Vagrant



🌞 Ajouter dans le `.gitlab-ci.yml` l'exécution de ces commandes afin de vérifier les fichiers de votre dépôt Git
